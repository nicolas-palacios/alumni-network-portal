import React, {useState} from 'react';
import {Button, Drawer, Form, Input, Select} from 'antd';
import '../../App.css'
import TextArea from "antd/es/input/TextArea";
import {addNewGroup} from "../../api/user";
import {successNotification} from "../../Notification";
import {Option} from "antd/es/mentions";

function NewGroupDrawer({openNewGroup, setOpenNewGroup, fetchGroups}) {

    const [placement, setPlacement] = useState('right');
    const {TextArea} = Input;
    const [submitting, setSubmitting] = useState(false);

    const showDrawer = () => {
        setOpenNewGroup(true);
    };
    const onClose = () => {
        setOpenNewGroup(false);
    };

    const onFinish = newGroup => {
        setSubmitting(true)
        addNewGroup(newGroup).then(() => {
            onClose();
            successNotification("Post sended")
            fetchGroups()
        }).catch(err => {
            console.log(err)
        }).finally(() => {
            setSubmitting(false)
        })
    };

    return (
        <>
            <Drawer
                title="Post"
                placement={placement}
                closable={false}
                onClose={onClose}
                open={openNewGroup}
                key={placement}
            >
                <Form
                    onFinish={onFinish}
                > <Form.Item name="fullName" label="Name">
                    <Input/>
                </Form.Item>
                    <Form.Item
                        name="isPrivate"
                        label="Private"
                    >
                        <Select>
                            <Option value="true">Private Group</Option>
                            <Option value="false">Open Group</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item name="description" label="Description">
                        <TextArea rows={4}/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">Submit</Button>
                    </Form.Item>
                </Form>
            </Drawer>
        </>
    );
};
export default NewGroupDrawer;