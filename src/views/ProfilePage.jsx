import {useEffect, useState} from "react";
import {getUserById} from "../api/user";

function ProfilePage() {
    const [user, setUser] = useState([]);


    useEffect(() => {
        getUserById().then(res => res.json())
            .then(data =>
                setUser(data))
    }, [])

    return (
        <>
            <section> {user.picture}</section>

            <section className="pt-16 bg-gray-200">
                <div class="w-full lg:w-4/12 px-4 mx-auto">
                    <div
                        className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg mt-16">
                        <div class="px-6">
                            <div class="flex flex-wrap justify-center">

                                <div class="w-full px-4 text-center mt-20">
                                    <div class="flex justify-center py-4 lg:pt-4 pt-8">
                                    </div>
                                </div>
                            </div>

                            <div class="text-center mt-12">


                                <div className="w-full px-4 flex justify-center">
                                    <div className="relative">
                                        <img className="rounded-2xl h-48 w-48" alt="..."
                                             src={user.picture === null ? 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png' : user.picture}/>
                                    </div>
                                </div>


                                <h3 className="text-xl font-semibold leading-normal mb-2 text-blueGray-700 mb-2 mt-5">
                                    {user.name}
                                </h3>


                                <div class="mb-2 text-blueGray-600 mt-10">

                                    <div className="opacity-60 mt-10">
                                        <p> Status</p>
                                    </div>
                                    <p className="mb-4 text-lg leading-relaxed text-blueGray-700">
                                        {user.status === null ? 'No fun fact' : user.status}
                                    </p>

                                    <i class="fas fa-briefcase mr-2 text-lg text-blueGray-400"></i>

                                </div>

                                <div className="opacity-60 mt-14">
                                    <p> Fun fact</p>
                                </div>

                                <p className="mb-4 text-lg leading-relaxed text-blueGray-700">
                                    {user.funFact === null ? 'No fun fact' : user.funFact}
                                </p>
                            </div>
                            <div class=" py-10 border-t border-blueGray-200 text-center">
                                <div class="flex flex-wrap justify-center">
                                    <div class="w-full lg:w-9/12 px-4">
                                        <div className="opacity-60 ">
                                            <p> Bio</p>
                                        </div>
                                        <p class="mb-4 text-lg leading-relaxed text-blueGray-700">
                                            {user.bio === null ? 'No bio' : user.bio}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default ProfilePage;
