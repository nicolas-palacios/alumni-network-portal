import GroupCard from "../Cards/GroupCard";
import TopicCard from "../Cards/TopicCard";
import keycloak from "../../keycloak";

const List = () => {

    const reguser = () => {
        fetch(`https://app-alumni-backend.herokuapp.com/api/v1/users/register`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                'Authorization': 'Bearer ' + keycloak.token
            }
        }).then(r => r.json()).then(data => console.log(data))
    }

    return (
        <>
<div className="flex justify-center">
    <button  onClick={() => reguser()} className="mt-5  inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">
        Please confirm your account</button>
</div>

                <div className="flex justify-center">


                            <table className="table-auto  ">

                    <tbody >
                    <h1 className="mt-32 shadow-blue-300 opacity-80 flex justify-center inline-block text-5xl sm:text-5xl font-bold text-slate-900 tracking-tight dark:text-slate-200">Groups</h1>
                    <tr className=" border-opacity-10 rounded-2xl shadow block  border-2 border-gray-200 px-56 pt-5 pb-5">
                        <td><GroupCard/></td>
                    </tr>
                    </tbody>
                    <h1 className="mt-32 shadow-blue-300 opacity-80 flex justify-center inline-block text-5xl sm:text-5xl font-bold text-slate-900 tracking-tight dark:text-slate-200">Topics</h1>
                    <tbody>

                    <tr className=" border-opacity-10 rounded shadow block  border-2 border-gray-200 px-56 pt-5 pb-5">
                        <td><TopicCard/></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </>
    )
}
export default List;