import React, {useState} from "react";
import {Button, Drawer, Form, Input} from "antd";
import {addNewTopic, getTopics} from "../../api/user";
import {successNotification} from "../../Notification";


function NewTopicDrawer({openNewTopic, setOpenNewTopic}) {
    const [placement, setPlacement] = useState('bottom');
    const {TextArea} = Input;
    const [submitting, setSubmitting] = useState(false);
    const showDrawer = () => {
        setOpenNewTopic(true);
    };
    const onClose = () => {
        setOpenNewTopic(false);
    };
    const onFinish = newTopic => {
        console.log(newTopic)
        setSubmitting(true)
        addNewTopic(newTopic)
            .then(() => {
                onClose()
                successNotification("New topic added")
            }).catch(err => {
            console.log(err)
        }).finally(() => {
            getTopics()
            setSubmitting(false)
        })
    }
    return (
        <>
            <Drawer
                title="Topic"
                placement={placement}
                closable={false}
                onClose={onClose}
                open={openNewTopic}
                key={placement}
            >
                <Form
                    onFinish={onFinish}
                >
                    <Form.Item name="name" label="Name">
                        <Input type="text"/>
                    </Form.Item>
                    <Form.Item name="description" label="Topic">
                        <Input type="text"/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">Submit</Button>
                    </Form.Item>
                </Form>
            </Drawer>
        </>
    )
}
export default NewTopicDrawer;