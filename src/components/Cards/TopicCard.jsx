import React, {useEffect, useState} from "react";
import {getTopics} from "../../api/user";
import {useNavigate} from "react-router-dom";
import FollowTopicButton from "../Buttons/FollowTopicButton";

const TopicCard = () => {
     const [topics, setTopics] = useState([]);

    const navigate = useNavigate();

    useEffect(() => {
        getTopics()
            .then(res => res.json())
            .then(data => {
                setTopics(data);
            })
    }, [])

    return (
        <>
            <ul>
                {topics.map(topic => {
                    return (
                        <li>
                            <div
                                className="flex bg-white shadow-lg rounded-lg mx-4 md:mx-auto my-10 max-w-md md:max-w-2xl ">
                                <div className="flex items-start px-4 py-6">
                                    <div className="">
                                        <div className="flex items-center justify-between">
                                            <h2 className="text-lg font-semibold text-gray-900 -mt-1"></h2>
                                            <small className="text-sm text-gray-700"></small>
                                        </div>
                                        <p className="text-gray-700">
                                            {topic.name}
                                        </p>
                                        <p className="mt-3 text-gray-700 text-sm">
                                            {topic.description}
                                        </p>
                                        <div className="mt-4 flex items-center">
                                            <div className="flex mr-2 text-gray-700 text-sm mr-3">
                                                <svg fill="none" viewBox="0 0 24 24" className="w-4 h-4 mr-1"
                                                     stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          stroke-width="2"
                                                          d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"/>
                                                </svg>
                                                <span></span>
                                            </div>
                                            <div className="flex mr-2 text-gray-700 text-sm mr-8">
                                                <svg fill="none" viewBox="0 0 24 24" className="w-4 h-4 mr-1"
                                                     stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          stroke-width="2"
                                                          d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z"/>
                                                </svg>
                                            </div>
                                            <div className="flex mr-2 text-gray-700 text-sm mr-4">
                                                <button type="button"
                                                        className=" mr-3 inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                                                        onClick={() => {
                                                            navigate(`/topicdetails/${topic.id}`)
                                                        }}
                                                >View
                                                </button>
                                                <FollowTopicButton topicId={topic.id}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    )
                })}

            </ul>

        </>

    )
}
export default TopicCard;