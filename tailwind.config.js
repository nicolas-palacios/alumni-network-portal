/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        'ThemeOne': 'E4C3AD',
        'themeTwo': 'FAE1DF',
        'themeThree': '9EA3B0',
        'themeFour': '546A7B',
        'themeFive': '0D1F2D'
      },
    },
  },
  plugins: [],
}
