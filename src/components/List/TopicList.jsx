import TopicCard from "../Cards/TopicCard";

const TopicList = () => {
    return (
        <>
            <div>
                <table>
                    <tbody>
                    <tr>
                        <TopicCard/>
                    </tr>
                    </tbody>
                </table>
            </div>
        </>
    )
}
export default TopicList;