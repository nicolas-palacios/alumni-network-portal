import TimelineSearchbar from "../components/Navbar/TimelineSearchbar";
import List from "../components/List/List";
import {useEffect} from "react";
import keycloak from "../keycloak";
import {getUserById} from "../api/user";

const HomePage = () => {




    return (
        <>
            <div className="mt-14">
                <TimelineSearchbar/>
                <List/>
            </div>


        </>
    )
}
export default HomePage;