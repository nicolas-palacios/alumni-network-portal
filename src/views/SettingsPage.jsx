import Settings from "../components/Settings/Settings";
import {useEffect, useState} from "react";
import {getUserById} from "../api/user";

const SettingsPage = () => {
    const [user, setUser] = useState([]);
    useEffect(() => {
        getUserById().then(res => res.json())
            .then(data =>
                setUser(data))
    }, [])

    return (
        <div>
            <Settings/>
        </div>
    )
}

export default SettingsPage;