import React, {useState} from 'react';
import {Button, Drawer, Form, Input} from 'antd';
import '../../App.css'
import TextArea from "antd/es/input/TextArea";
import {sendDm} from "../../api/user";
import {successNotification} from "../../Notification";

function PrivateMessageDrawer({dmOpen, setDmOpen, targetUid,}) {

    const [placement, setPlacement] = useState('bottom');
    const {TextArea} = Input;
    const [submitting, setSubmitting] = useState(false);

    const showDrawer = () => {
        setDmOpen(true);
    };

    const onClose = () => {
        setDmOpen(false);
    };

    const onFinish = newPost => {
        setSubmitting(true)
        sendDm(newPost, targetUid).then(() => {
            onClose();
            successNotification("DM sent")
        }).catch(err => {
            console.log(err)
        }).finally(() => {
            setSubmitting(false)
        })
    };

    return (
        <>
            <Drawer
                title="Post"
                placement={placement}
                closable={false}
                onClose={onClose}
                open={dmOpen}
                key={placement}
            >
                <Form
                    onFinish={onFinish}
                >
                    <Form.Item name="postMessage" label="Post message">
                        <TextArea rows={4}/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">Submit</Button>
                    </Form.Item>
                </Form>
            </Drawer>
        </>
    );
};
export default PrivateMessageDrawer;