import keycloak from "../keycloak";
import React, {useState} from 'react';

import {addNewGroup, getGroups, getUserById, getUsers, updateUser} from "../api/user";

function StartPage() {

    const [user, setUser] = useState([]);

    const appGroup =
        {
            id: 8,
            fullName: "Best group",
            description: "Best best group"
        }
    const appUser =
        {
            picture: "na na na",
            status: "singel",
            bio: "Na nam ",
            funFact: "Funny as hell"
        }
    console.log(user)
    return (

        <div>

            <section className="actions">
                {!keycloak.authenticated && (
                    <button onClick={() => keycloak.login()}>Login</button>
                )}
                {keycloak.authenticated && (
                    <button onClick={() => keycloak.logout()}>Logout</button>
                )}{keycloak.authenticated && (
                <div>
                    <button onClick={() =>
                        fetch(`https://app-alumni-backend.herokuapp.com/api/v1/users/register`, {
                            method: 'POST',
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                'Authorization': 'Bearer ' + keycloak.token
                            }
                        }).then(r => r.json()).then(data => console.log(data))

                    }>reg
                    </button>
                </div>
            )}
                {keycloak.authenticated && (
                    <div>
                        <button onClick={() =>
                            addNewGroup(appGroup)
                        }>regGroup
                        </button>
                    </div>
                )}
                {keycloak.authenticated && (
                    <div>
                        <button onClick={() =>
                            updateUser(appUser)
                        }>update user
                        </button>
                    </div>
                )}
                {keycloak.authenticated && (
                    <div>
                        <button onClick={() =>
                            getUserById().then(res => res.json())
                                .then(data =>
                                    setUser(data)).then(data => console.log(data))
                        }>getById
                        </button>
                    </div>
                )}
                {keycloak.authenticated && (
                    <div>
                        <button onClick={() =>
                            getGroups().then(res => res.json())
                                .then(data => {
                                    console.log(data);
                                })
                        }>get groups
                        </button>
                    </div>
                )}
                {keycloak.authenticated && (
                    <div>
                        <button onClick={() =>
                            getUsers().then(res => res.json())
                                .then(data => {
                                    console.log(data);
                                })
                        }>get users
                        </button>
                    </div>
                )}
            </section>
        </div>
    );
}

export default StartPage;
