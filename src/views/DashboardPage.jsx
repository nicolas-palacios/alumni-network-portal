import React from "react";
import PostCard from "../components/Cards/PostCard";

const DashboardPage = () => {
    return (
        <>
            <ul>
                <li>
                    <PostCard/>
                </li>
            </ul>
        </>
    )
}
export default DashboardPage;