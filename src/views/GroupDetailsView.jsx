import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {getGroupPosts} from "../api/user";
import GroupPostDrawer from "../components/Drawers/GroupPostDrawer";
import ReplyGroupDrawer from "../components/Drawers/ReplyGroupDrawer";

const GroupDetailsView = () => {
    const {id} = useParams();
    const [posts, setPosts] = useState([]);
    const [open, setOpen] = useState(false);
    const [openReply, setOpenReply] = useState(false);


    useEffect(() => {
        getGroupPosts(id)
            .then(res => res.json())
            .then(data => {
                setPosts(data)
            })
    }, []);

    const fetchPosts = () => {
        getGroupPosts(id)
            .then(res => res.json())
            .then(data => {
                setPosts(data)
            })
    };

    return <>
        <div className="flex justify-around w-full">

        <div className="flex justify-evenly flex-row w-1/3">


        <button  onClick={() => fetchPosts()} className="mt-14  inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">Update</button>
        <button  onClick={() => setOpen(!open)} className="mt-14  inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">New Post</button>
            </div>
        </div>

        <GroupPostDrawer open={open} setOpen={setOpen} groupId={id} fetchPosts={fetchPosts}/>
        <ul>
            {posts.sort((a, b) => b.id - a.id).map(post => {
                return (
                    <li>
                        <div
                            className="flex bg-white shadow-lg rounded-lg mx-4 md:mx-auto my-10 max-w-md md:max-w-2xl ">
                            <div className="flex items-start px-4 py-6">
                                {post.picture !== null && (
                                <img className="w-14 h-14 rounded-full object-cover mr-4 shadow"
                                     src= {post.picture}
                                     alt="avatar"/>
                                )}
                                {post.picture === null && (
                                    <img
                                        className="h-8 w-8 rounded-full"
                                        src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                                        alt=""
                                    />
                                )}
                                <div className="">
                                    <div className="flex items-center justify-between">
                                        <h10 className="text-lg font-semibold text-gray-900 -mt-1"> #{post.id}    {post.name}</h10>
                                        {post.answeredTo > 0 &&
                                            <small style={{paddingLeft: 300}} className="text-sm text-gray-700"> Replied
                                                on #{post.answeredTo}</small>
                                        }
                                    </div>
                                    <small>Posted: {post.createDateTime.substring(0,10)}</small> <span> <small> {post.createDateTime.substring(11,16)}</small></span>

                                    <p className="mt-5 text-gray-700 text-sm">
                                        {post.postMessage}

                                    </p>
                                    <div className="mt-4 flex items-center">
                                        <div className="flex mr-2 text-gray-700 text-sm mr-8">
                                            <svg fill="none" viewBox="0 0 24 24" className="cursor-pointer w-4 h-4 mt-0.5 mr-1"
                                                 stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z"/>
                                            </svg>
                                            <span  className="cursor-pointer rounded  focus:shadow-lg focus:outline-none focus:ring-amber-100 active:shadow-lg transition duration-150 ease-in-out" onClick={() => setOpenReply(!openReply)}>Answer</span>
                                            <ReplyGroupDrawer openReply={openReply} setOpenReply={setOpenReply}
                                                              groupId={id} postId={post.id} targetUid={post.senderUser}
                                                              fetchPosts={fetchPosts}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                )
            })}
        </ul>

    </>
}
export default GroupDetailsView;