import {Button, Drawer, Form} from "antd";
import React, {useEffect, useState} from "react";
import {findSentPostsToUser, getGroupPosts, getGroups} from "../../api/user";
import JoinButton from "../Buttons/JoinButton";


function DMDeatailView ({conversationDrawer,setConversationDrawer,targetId}) {
    const [placement, setPlacement] = useState('right');
    const [dmPosts, setDmPosts] = useState([]);



    const showDrawer = () => {
        setConversationDrawer(true);
    };


    const onClose = () => {
        setConversationDrawer(false);
    };

    // useEffect(() => {
    //     findSentPostsToUser(targetId)
    //         .then(res => res.json())
    //         .then(data => {
    //             setDmPosts(data)
    //         })
    // }, []);

    return (
        <>
            <Drawer
                title="Post"
                placement={placement}
                closable={false}
                onClose={onClose}
                open={conversationDrawer}
                key={placement}
            >


                return (
                <ul>
                    {dmPosts.map(dmPost => {
                        return (
                            <li>
                                <div className="flex justify-center mt-14">
                                    <div className="rounded-lg shadow-lg bg-white max-w-sm">
                                        {/*Todo länka till posten och trådar*/}
                                        <div className="p-6">
                                            {/*Todo Att sätta in en variabel som tar in namnet på användaren*/}
                                            <h5 className="text-gray-900 text-xl font-medium mb-2">{dmPost.name}</h5>
                                            <small>{dmPost.createDateTime}</small>
                                            {/*Todo att sätta in en varbael som tar posten/eventen*/}
                                            <p className="text-gray-700 text-base mb-4">
                                                {dmPost.postMessage}
                                            </p>
                                            {/*Todo att sätta in vilken grupp/audience*/}
                                        </div>
                                    </div>
                                </div>
                            </li>
                        );
                    })}
                </ul>

            </Drawer>
        </>
    );
};


export default DMDeatailView;