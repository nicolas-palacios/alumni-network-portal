import JoinButton from "../Buttons/JoinButton";
import {getGroups} from "../../api/user";
import {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import keycloak from "../../keycloak";

const GroupCard = () => {
    const navigate = useNavigate();
    const [groups, setGroups] = useState([]);


    useEffect(() => {
        getGroups()
            .then(res => res.json())
            .then(data => {
                setGroups(data)
                console.log(data)
            })
    }, []);


    return (
        <ul>
            {groups.map(group => {
                return (
                    <li>
                        <div className="flex justify-center mt-14">
                            <div className=" rounded-lg shadow-lg bg-white max-w-sm">
                                <a href="http://localhost:3000/postdetails">
                                    <img className="rounded-t-lg"
                                         src="https://mdbootstrap.com/img/new/standard/nature/184.jpg"
                                         alt=""/>
                                </a>
                                <div className="p-6">
                                    <h5 className="text-gray-900 text-xl font-medium mb-2">{group.fullName}</h5>
                                    <p className="text-gray-700 text-base mb-4">
                                        {group.description}
                                    </p>
                                    <p className="text-gray-900 text-xl font-medium mb-2">Group Gamers</p>
                                    <button type="button"
                                            className=" mr-3 inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                                            onClick={() => {
                                                navigate(`/groupdetails/${group.id}`)
                                            }}>View
                                    </button>
                                    <JoinButton groupId={group.id}/>

                                </div>
                            </div>
                        </div>
                    </li>
                );
            })}
        </ul>
    )
};
export default GroupCard;
