import {joinTopic} from "../../api/user";
import {errorNotification, successNotification} from "../../Notification";

const FollowTopicButton = ({topicId}) => {
    return (
        <button type="button"
                className=" inline-block px-6 py-2.5 bg-blue-600 text-white
                 font-medium text-xs leading-tight uppercase rounded shadow-md
                 hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none
                 focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                onClick={() => {
                    joinTopic(topicId).then(() => {
                        successNotification("You are now following this topic");
                    }).catch(err => {
                        err.response.json().then(res => {
                            console.log(res);
                            errorNotification("There was an issue",
                                `${res.message} [${res.status}] [${res.error}]`)
                        })
                    })
                }}>Follow
        </button>
    )
}
export default FollowTopicButton;