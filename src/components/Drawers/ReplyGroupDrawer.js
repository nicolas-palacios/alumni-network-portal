import React, {useState} from 'react';
import {Button, Drawer, Form, Input} from 'antd';
import '../../App.css'
import TextArea from "antd/es/input/TextArea";
import {answerToGroupPosts} from "../../api/user";
import {successNotification} from "../../Notification";

function ReplyGroupDrawer({openReply, setOpenReply, groupId, postId, targetUid, fetchPosts}) {

    const [placement, setPlacement] = useState('bottom');
    const {TextArea} = Input;
    const [submitting, setSubmitting] = useState(false);

    const showDrawer = () => {
        setOpenReply(true);
    };
    const onClose = () => {
        setOpenReply(false);
    };

    const onFinish = newPost => {
        setSubmitting(true)
        answerToGroupPosts(newPost, groupId, postId, targetUid).then(() => {
            onClose();
            successNotification("Post sent")
            fetchPosts()
        }).catch(err => {
            console.log(err)
        }).finally(() => {
            setSubmitting(false)
        })
    };

    return (
        <>
            <Drawer
                title="Post"
                placement={placement}
                closable={false}
                onClose={onClose}
                open={openReply}
                key={placement}
            >
                <Form
                    onFinish={onFinish}
                >
                    <Form.Item name="postMessage" label="Post message">
                        <TextArea rows={4}/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">Submit</Button>
                    </Form.Item>
                </Form>
            </Drawer>
        </>
    );
};
export default ReplyGroupDrawer;