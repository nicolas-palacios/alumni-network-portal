import React, {useState} from 'react';
import {Button, Drawer, Form, Input} from 'antd';
import '../../App.css'
import TextArea from "antd/es/input/TextArea";
import {addNewGroupPost} from "../../api/user";
import {successNotification} from "../../Notification";

function GroupPostDrawer({open, setOpen, groupId, fetchPosts}) {

    const [placement, setPlacement] = useState('bottom');
    const {TextArea} = Input;
    const [submitting, setSubmitting] = useState(false);

    const showDrawer = () => {
        setOpen(true);
    };

    const onClose = () => {
        setOpen(false);
    };

    const onFinish = newPost => {
        setSubmitting(true)
        addNewGroupPost(newPost, groupId).then(() => {
            onClose();
            successNotification("Post sended")
            fetchPosts()
        }).catch(err => {
            console.log(err)
        }).finally(() => {
            setSubmitting(false)
        })
    };

    return (
        <>
            <Drawer
                title="Post"
                placement={placement}
                closable={false}
                onClose={onClose}
                open={open}
                key={placement}
            >
                <Form
                    onFinish={onFinish}
                >
                    <Form.Item name="postMessage" label="Post message">
                        <TextArea rows={4}/>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">Submit</Button>
                    </Form.Item>
                </Form>
            </Drawer>
        </>
    );
};
export default GroupPostDrawer;