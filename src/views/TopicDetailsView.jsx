import React, {useEffect, useState} from "react";
import {getTopicsPosts} from "../api/user";
import {useParams} from "react-router-dom";
import TopicReplyDrawer from "../components/Drawers/TopicReplyDrawer";
import TopicPostDrawer from "../components/Drawers/TopicPostDrawer";


const TopicDetailsView = () => {
    const {id} = useParams();
    const [topicPost, setTopicPost] = useState([]);
    const [openTopicPost, setOpenTopicPost] = useState(false);
    const [openReply, setOpenReply] = useState(false);

    useEffect(() => {
        getTopicsPosts(id)
            .then(res => res.json())
            .then(data => {
                setTopicPost(data)
            })
    }, []);

    const fetchPosts = () => {
        getTopicsPosts(id)
            .then(res => res.json())
            .then(data => {
                setTopicPost(data);
            })
    };


    return (
        <>
            <div className="flex justify-around w-full">
                <div className="flex justify-evenly flex-row w-1/3">
            <button onClick={() => fetchPosts()}  className="mt-14  inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">Update </button>
            <button onClick={() => setOpenTopicPost(!openTopicPost)}  className="mt-14  inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out">Open</button>
            </div>
            </div>
            <TopicPostDrawer openTopicPost={openTopicPost} setOpenTopicPost={setOpenTopicPost} topicId={id}
                             fetchPosts={fetchPosts}/>
            <ul>
                {topicPost.sort((a, b) => b.id - a.id).map( post => {
                    return (
                        <li>
                            <div
                                className="flex bg-white shadow-lg rounded-lg mx-4 md:mx-auto my-10 max-w-md md:max-w-2xl ">
                                <div className="flex items-start px-4 py-6">
                                    {post.picture !== null && (
                                        <img className="w-14 h-14 rounded-full object-cover mr-4 shadow"
                                             src= {post.picture}
                                             alt="avatar"/>
                                    )}
                                    {post.picture === null && (
                                        <img
                                            className="h-8 w-8 rounded-full"
                                            src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                                            alt=""
                                        />
                                    )}
                                    <div className="">
                                        <div className="flex items-center justify-between">
                                            <h10 className="text-lg font-semibold text-gray-900 -mt-1"> #{post.id}    {post.name}</h10>
                                            {post.answeredTo > 0 &&
                                                <small style={{paddingLeft: 300}}
                                                       className="text-sm text-gray-700"> Replied
                                                    on post {post.answeredTo}</small>
                                            }

                                        {/* 0-10, 10-16    */}

                                        </div>
                                        <small>Posted: {post.createDateTime.substring(0,10)}</small> <span> <small> {post.createDateTime.substring(11,16)}</small></span>
                                        <p className="mt-5 text-gray-700 text-sm">
                                            {post.postMessage}

                                        </p>
                                        <div className="mt-4 flex items-center">
                                            <div className="flex mr-2 text-gray-700 text-sm mr-8">

                                                <span onClick={() => setOpenReply(!openReply)}>Answer</span>
                                                <TopicReplyDrawer openReply={openReply} setOpenReply={setOpenReply}
                                                                  topicId={id} postId={post.id}
                                                                  targetUid={post.senderUser}
                                                                  fetchPosts={fetchPosts}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    )
                })}
            </ul>
        </>
    )
}
export default TopicDetailsView;