import React, {useState} from "react";
import {Button, Drawer, Form, Input} from "antd";
import {addNewTopic, addNewTopicPost} from "../../api/user";
import {successNotification} from "../../Notification";

function TopicPostDrawer({openTopicPost, setOpenTopicPost, fetchTopics, topicId}) {
    const [placement, setPlacement] = useState('bottom');
    const {TextArea} = Input;
    const [submitting, setSubmitting] = useState(false);

    const showDrawer = () => {
        setOpenTopicPost(true);
    };

    const onClose = () => {
        setOpenTopicPost(false);
    };

    const onFinish = newTopic => {
        console.log(newTopic)
        setSubmitting(true)
        addNewTopicPost(newTopic, topicId)
            .then(() => {
                onClose()
                successNotification("Post successfully sent!")
                fetchTopics()
            }).catch(err => {
            console.log(err)
        }).finally(() => {
            setSubmitting(false)
        })
    }

    return (
        <>
            <Drawer
                title="Topic"
                placement={placement}
                closable={false}
                onClose={onClose}
                open={openTopicPost}
                key={placement}
            >
                <Form
                    onFinish={onFinish}
                >
                    <Form.Item name="postMessage" label="Post message">
                        <TextArea rows={4}/>
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit">Submit</Button>
                    </Form.Item>
                </Form>
            </Drawer>
        </>
    )
}
export default TopicPostDrawer;