import keycloak from "../keycloak";
import axios from "axios";


/**
 * SAMPLE FUNCTION: Create a new user on the database
 * @returns { Promise<{user: any, error: string | null}> } user
 */

export const createProfile = async (user) => {
    try {
        const {data} = await axios.get("https://app-alumni-backend.herokuapp.com/api/v1/users/user", {
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                'Authorization': 'Bearer ' + keycloak.token
            },
            data: user,
        });
        return Promise.resolve({
            user: data,
            error: null,
        });
    } catch (e) {
        return Promise.reject({
            error: e.message,
            user: null,
        });
    }
};


const checkStatus = response => {
    if (response.ok) {

        return response;
    }
    console.log(response + " " + "test")
    const error = new Error(response.statusText);
    error.response = response;
    return Promise.reject(error);
}


export const getUsers = () =>
    fetch("https://app-alumni-backend.herokuapp.com/api/v1/users/get", {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + keycloak.token
            }
        }
    ).then(checkStatus)

export const getPosts = () =>
    fetch("https://app-alumni-backend.herokuapp.com/api/v1/post/get", {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + keycloak.token
            }
        }
    ).then(checkStatus)

export const getGroups = () =>
    fetch("https://app-alumni-backend.herokuapp.com/api/v1/groups/get", {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + keycloak.token
            }
        }
    ).then(checkStatus)

export const getUserById = () =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/users/user`, {
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                'Authorization': 'Bearer ' + keycloak.token
            }
        }
    ).then(checkStatus)

export const getGroupPosts = id =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/post/group/${id}`, {
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                'Authorization': 'Bearer ' + keycloak.token
            }
        }
    ).then(checkStatus)

export const addNewGroup = (appGroup) =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/groups`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'POST',
        body: JSON.stringify(appGroup)
    }).then(checkStatus)

export const addNewGroupPost = (newPost, groupId) =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/post/grouppost/${groupId}`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'POST',
        body: JSON.stringify(newPost)
    }).then(checkStatus)

export const answerToGroupPosts = (newPost, groupId, postId, targetUid) =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/post/group/${groupId}/post/${postId}/user/${targetUid}`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'POST',
        body: JSON.stringify(newPost)
    }).then(checkStatus)

export const sendDm = (newPost, targetUid) =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/post/targetUser/${targetUid}`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'POST',
        body: JSON.stringify(newPost)
    }).then(checkStatus)


export const addNewTopic = (newTopic) =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/topic`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'POST',
        body: JSON.stringify(newTopic)
    }).then(checkStatus)

export const getTopics = () =>
    fetch("https://app-alumni-backend.herokuapp.com/api/v1/topic/get", {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + keycloak.token
            }
        }
    ).then(checkStatus)

export const joinTopic = topicId =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/topic/follow/${topicId}`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'PUT'
    }).then(checkStatus)

export const getTopicsPosts = topicId =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/post/topic/${topicId}`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        }
    }).then(checkStatus)

export const answerToTopicPost = (newPost, topicId, postId, targetUid) =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/post/topic/${topicId}/post/${postId}/user/${targetUid}`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'POST',
        body: JSON.stringify(newPost)
    }).then(checkStatus)

export const addNewTopicPost = (newPost, topicId) =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/post/topicpost/${topicId}`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'POST',
        body: JSON.stringify(newPost)
    }).then(checkStatus)

export const updateUser = (appUser) =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/users/updateinfo`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'PUT',
        body: JSON.stringify(appUser)
    }).then(checkStatus)

export const joinGroup = groupId =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/users/joingroup/${groupId}`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'PUT'
    }).then(checkStatus)

export const findSentPostsToUser = targetUid =>
    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/post/conversation/${targetUid}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + keycloak.token
            }
        }
    ).then(checkStatus)

export const findReceivedPosts = () =>
    fetch("https://app-alumni-backend.herokuapp.com/api/v1/post/user", {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + keycloak.token
            }
        }
    ).then(checkStatus)




