import React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import KeycloakRoute from "./routes/KeycloakRoute";
import './App.css';
import Navbar from "./components/Navbar/Navbar";
import StartPage from "./views/StartPage";
import {ROLES} from "./const/roles";
import ProfilePage from "./views/ProfilePage";
import SettingsPage from "./views/SettingsPage";
import HomePage from "./views/HomePage";
import GroupDetailsView from "./views/GroupDetailsView";
import GroupPage from "./views/GroupPage";
import DashboardPage from "./views/DashboardPage";
import TopicPage from "./views/TopicPage";
import TopicDetailsView from "./views/TopicDetailsView";
import MembersPage from "./views/MembersPage";


function App() {
    return (


        // Change the background color here
        <div className=" min-h-screen bg-gradient-to-b from-indigo-50 to-indigo-100">
        <BrowserRouter >

            <Navbar/>

            <main className=" ">
                <Routes>
                    <Route path={'/'} element={<HomePage/>}></Route>
                    <Route
                        path="/profile"
                        element={
                            <KeycloakRoute role={ROLES.User}>
                                <ProfilePage/>
                            </KeycloakRoute>
                        }
                    />
                    <Route path="/settings" element={<SettingsPage/>}/>
                    <Route path="/dashboard" element={<DashboardPage/>}/>
                    <Route path={"/groupdetails/:id"} element={<GroupDetailsView/>}></Route>
                    <Route path={"/profilepage"} element={<ProfilePage/>}></Route>
                    <Route path={"/groups"} element={<GroupPage/>}></Route>
                    <Route path={"/topics"} element={<TopicPage/>}></Route>
                    <Route path={"/topicdetails/:id"} element={<TopicDetailsView/>}></Route>
                    <Route path={"/members"} element={<MembersPage/>}></Route>

                </Routes>

            </main>

        </BrowserRouter>
        </div>


    );
}

export default App;
