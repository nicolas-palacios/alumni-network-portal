import React, {useState} from "react";
import TopicCard from "../components/Cards/TopicCard";
import NewTopicDrawer from "../components/Drawers/NewTopicDrawer";
import {getTopics} from "../api/user";

const TopicPage = () => {
    const [openNewTopic, setOpenNewTopic] = useState(false);

    return (
        <>
            <div className="flex justify-center">
                <button onClick={() => setOpenNewTopic(!openNewTopic)} type="button"
                        className=" flex justify-center mt-10 ml-10 inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                >Create new topic
                </button>
            </div>
            <NewTopicDrawer openNewTopic={openNewTopic} setOpenNewTopic={setOpenNewTopic}/>
            <ul>
                <li>
                    <TopicCard/>
                </li>
            </ul>
        </>
    )
}
export default TopicPage;