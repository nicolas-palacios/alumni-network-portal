import React, {useEffect, useState} from 'react';
import {getUsers} from "../api/user";
import PrivateMessageDrawer from "../components/Drawers/PrivateMessageDrawer";

const MembersPage = () => {

    const [members, setMembers] = useState([]);
    const [dmOpen, setDmOpen] = useState(false);
    const [targetUid, setTargetUid] = useState(null);


    useEffect(() => {
        getUsers()
            .then(res => res.json())
            .then(data => {
                setMembers(data)
            })
    }, []);

    const fetchUsers = () => {
        getUsers()
            .then(res => res.json())
            .then(data => {
                setMembers(data)
            })
    };

    //     <!--
    //                     <li>
    //                         <table className="min-w-full table-auto">
    //                             <tbody className="bg-gray-200 ">
    //                             <tr className="bg-white border-4 w-10 border-gray-200 ">
    //                                 <td>
    //                                     <span className="text-center mx-2 ml-2 font-semibold">{member.name}</span>
    //                                 </td>
    //                                 <td className="px-16 py-2 text-e<">
    //                                     <span>{member.bio}</span>
    //                                 </td>
    //                                 <td className="px-16 py-2">
    //                                     <button onClick={() => {
    //                                         setTargetUid(member.uid)
    //                                         setDmOpen(!dmOpen)
    //                                     }}
    //                                             className="bg-indigo-500 text-white px-4 py-2 border rounded-md hover:bg-white hover:border-indigo-500 hover:text-black float-right">
    //                                         Send DM
    //                                     </button>
    //                                     <PrivateMessageDrawer dmOpen={dmOpen} setDmOpen={setDmOpen}
    //                                                           targetUid={targetUid}/>
    //                                 </td>
    //                             </tr>
    //                             </tbody>
    //                         </table>
    //                     </li>
    // -->

    return (
        <ul>
            {members.map(member => {
                return (
                    <>

                            <li>
                                <table className="min-w-full table-auto">
                                    <tbody className="bg-gray-200 ">
                                    <tr className="bg-white border-4 border-gray-200 flex  flex-row justify-between">
                                        <td className=" w-full px-3 w-20">
                                            <span className="font-semibold">{member.name}</span>
                                        </td>
                                        <td className=" py-2 w-1/2">
                                            <span>{member.bio}</span>
                                        </td>
                                        <td className="py-2 ">
                                            <button onClick={() => {
                                                setTargetUid(member.uid)
                                                setDmOpen(!dmOpen)
                                            }}
                                                    className="bg-indigo-500 text-white px-4 py-2 border rounded-md hover:bg-white hover:border-indigo-500 hover:text-black float-right">
                                                Send DM
                                            </button>
                                            <PrivateMessageDrawer dmOpen={dmOpen} setDmOpen={setDmOpen}
                                                                  targetUid={targetUid}/>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </li>
                    </>

            )
            }
            )
            }
            </ul>

            )
            }
            export default MembersPage;