import React, {useEffect, useState} from "react";
import {findReceivedPosts} from "../../api/user";
import PrivateMessageDrawer from "../Drawers/PrivateMessageDrawer";
import {useNavigate} from "react-router-dom";

const PostCard = () => {
    const [posts, setPost] = useState([]);
    const navigate = useNavigate();
    const [dmOpen, setDmOpen] = useState(false);
    const [targetUid, setTargetUid] = useState(null);

    useEffect(() => {
        findReceivedPosts()
            .then(res => res.json())
            .then(data => {
                setPost(data)
            })
    }, []);

    return (
        <>
            <ul>
                {posts.sort((a, b) => b.id - a.id).map(post => {
                    return (
                        <li>
                            <div
                                className="flex bg-white shadow-lg rounded-lg mx-4 md:mx-auto my-10 max-w-md md:max-w-2xl ">
                                <div className="flex items-start px-4 py-6">
                                    {post.picture !== null && (
                                        <img className="w-14 h-14 rounded-full object-cover mr-4 shadow"
                                             src= {post.picture}
                                             alt="avatar"/>
                                    )}            {post.picture === null && (
                                    <img
                                        className="h-8 w-8 rounded-full"
                                        src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
                                        alt=""
                                    />
                                )}
                                    <div>
                                        <div className="flex items-center justify-between">
                                            <h2 className="text-lg font-semibold text-gray-900 -mt-1"></h2>
                                            <small className="text-sm text-gray-700"></small>
                                        </div>
                                        <h10 className="text-lg font-semibold text-gray-900 -mt-1">{post.name}</h10>

                                        <p className="mt-3 text-gray-700 text-sm">

                                            {post.postMessage}
                                        </p>
                                        <div className="mt-4 flex items-center">
                                            <div className="flex mr-2 text-gray-700 text-sm mr-3">
                                                <svg fill="none" viewBox="0 0 24 24" className="w-4 h-4 mr-1"
                                                     stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          stroke-width="2"
                                                          d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"/>
                                                </svg>
                                                <span></span>
                                            </div>
                                            <div className="flex mr-2 text-gray-700 text-sm mr-8">
                                                <svg fill="none" viewBox="0 0 24 24" className="w-4 h-4 mr-1"
                                                     stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          stroke-width="2"
                                                          d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z"/>
                                                </svg>
                                            </div>

                                            {post.targetGroup === 0 && post.targetTopic === 0 && (
                                                <button type="button"
                                                        className=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                                                        onClick={() => {
                                                            setTargetUid(post.senderUser)
                                                            setDmOpen(!dmOpen)
                                                        }}>
                                                    Answer DM
                                                </button>
                                            )}

                                            <PrivateMessageDrawer dmOpen={dmOpen} setDmOpen={setDmOpen}
                                                                  targetUid={targetUid}/>
                                            {post.targetGroup > 0 && (
                                                <button type="button"
                                                        className=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                                                        onClick={() => {
                                                            navigate(`/groupdetails/${post.targetGroup}`)
                                                        }}>
                                                    Go to group
                                                </button>
                                            )}
                                            {post.targetTopic > 0 && (
                                                <button type="button"
                                                        className=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                                                        onClick={() => {
                                                            navigate(`/topicdetails/${post.targetTopic}`)
                                                        }}>
                                                    Go to topic
                                                </button>
                                            )}
                                            <div className="flex mr-2 text-gray-700 text-sm mr-4">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                    )
                })}

            </ul>

        </>

    )

}
export default PostCard;


