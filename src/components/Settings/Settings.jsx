import {getUserById} from "../../api/user";
import React, {useEffect, useState} from "react";
import keycloak from "../../keycloak";
import {Form, Select} from "antd";
import {Option} from "antd/es/mentions";

function Update (user, fname, lname ) {

    const appUser =
        {
            name: fname + " " + lname,
            picture: user.picture,
            status: user.status,
            bio: user.bio,
            funFact: user.funFact
        }

    fetch(`https://app-alumni-backend.herokuapp.com/api/v1/users/updateinfo`, {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            'Authorization': 'Bearer ' + keycloak.token
        },
        method: 'PUT',
        body: JSON.stringify(appUser)
    })
        .then()
}

function testPic (pic) {
    console.log(pic)
}

const Settings = () => {

    const [user, setUser] = useState([]);
    const [fName, setFName] = useState("");
    const [lName, setLName] = useState("");

    useEffect(() => {
        getUserById().then(res => res.json())
            .then(data => {
                    setFName(data.name.split(" ")[0])
                    setLName(data.name.split(" ")[1])
                    setUser(data)
                }
            )
    }, [])

    // setUser(prev => ({
    //     ...prev,
    //     ["picture"]: event.target.value
    // }))

    return (

        <div class="bg-gray-200 min-h-screen pt-2 font-mono my-16">
            <div class="container mx-auto">
                <div class="inputs w-full max-w-2xl p-6 mx-auto">
                    <h2 class="text-2xl text-gray-900">Account Settings</h2>
                    <form class="mt-6 border-t border-gray-400 pt-4">
                        <div class='flex flex-wrap -mx-3 mb-6'>
                            <div class='w-full md:w-full px-3 mb-6'>
                                <label class='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2'
                                       for='grid-text-1'>First name</label>
                                <input
                                    class='appearance-none block w-full bg-white text-gray-700 border border-gray-400 shadow-inner rounded-md py-3 px-4 leading-tight focus:outline-none  focus:border-gray-500'
                                    onInput={(event) => setFName(event.target.value)} id='grid-text-1' type='text'
                                    placeholder={fName} required/>
                            </div>

                            <div className='w-full md:w-full px-3 mb-6'>
                                <label className='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2'
                                       htmlFor='grid-text-1'>Last name</label>
                                {/*TODO: Image*/}
                                <input
                                    className='appearance-none block w-full bg-white text-gray-700 border border-gray-400 shadow-inner rounded-md py-3 px-4 leading-tight focus:outline-none  focus:border-gray-500'
                                    onInput={(event) => setLName(event.target.value)} id='grid-text-1' type='text'
                                    placeholder={lName} required/>
                            </div>
                            <div class='w-full md:w-full px-3 mb-6 '>
                                <Form.Item
                                    name="picture"
                                    label="Profile Picture"
                                >
                                    <select onChange={ (event) => setUser(prev => ({
                                             ...prev,
                                             ["picture"]: event.target.value
                                         }))} >

                                        <option value="https://www.nicepng.com/png/detail/136-1362504_marge-simpson.png">Woman</option>
                                        <option value="https://w7.pngwing.com/pngs/480/557/png-transparent-bart-simpsons-illustration-homer-simpson-lisa-simpson-marge-simpson-fox-satire-homer-television-face-animals.png">Man</option>
                                        <option value="https://whatsondisneyplus.com/wp-content/uploads/2021/09/lisa.png">Girl</option>
                                        <option value="https://e7.pngegg.com/pngimages/796/360/png-clipart-bart-simpson-homer-simpson-lisa-simpson-the-simpsons-road-rage-marge-simpson-bart-simpson-face-text-thumbnail.png">Boy</option>

                                    </select>
                                </Form.Item>
                                <label
                                    class='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2'>Profile
                                    picture</label>
                                <img className=" w-48 h-48 opacity-70"
                                     src={user.picture === null ? 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png' : user.picture}
                                     alt="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"/>

                            </div>

                            <div class="personal w-full border-t border-gray-400 pt-4">
                                <h2 class="text-2xl text-gray-900">Personal info:</h2>
                                <div className='w-full md:w-full px-3 mb-6'>
                                    <label
                                        className='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2 mt-5'>
                                        Status</label>
                                    <textarea
                                        className='bg-gray-100 rounded-md border leading-normal resize-none w-full h-20 py-2 px-3 shadow-inner border border-gray-400 font-medium placeholder-gray-700 focus:outline-none focus:bg-white'
                                        placeholder={user.status === null ? ' ' : user.status}
                                        onChange={(event) => setUser(prev => ({
                                            ...prev,
                                            ["status"]: event.target.value
                                        }))}
                                        required></textarea>
                                </div>

                                <div class='w-full md:w-full px-3 mb-6 mt-4'>
                                    <label
                                        class='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2'>BIO</label>
                                    <textarea
                                        className='bg-gray-100 rounded-md border leading-normal resize-none w-full h-20 py-2 px-3 shadow-inner border border-gray-400 font-medium placeholder-gray-700 focus:outline-none focus:bg-white'
                                        placeholder={user.bio === null ? ' ' : user.bio}
                                        onChange={(event) => setUser(prev => ({...prev, ["bio"]: event.target.value}))}
                                        required></textarea>
                                </div>
                                <div class='w-full md:w-full px-3 mb-6'>
                                    <label
                                        class='block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2'>FUN
                                        FACT</label>
                                    <textarea
                                        class='bg-gray-100 rounded-md border leading-normal resize-none w-full h-20 py-2 px-3 shadow-inner border border-gray-400 font-medium placeholder-gray-700 focus:outline-none focus:bg-white'
                                        placeholder={user.funFact === null ? ' ' : user.funFact}
                                        onChange={(event) => setUser(prev => ({
                                            ...prev,
                                            ["funFact"]: event.target.value
                                        }))}
                                        required></textarea>
                                </div>

                                <div class="flex justify-end">
                                    <button
                                        class="appearance-none bg-gray-200 text-gray-900 px-2 py-1 shadow-sm border border-gray-400 rounded-md mr-3"
                                        type="button" onClick={() => Update(user, fName, lName)}>save changes
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default Settings;