
import {useEffect, useState} from "react";
import {getGroups} from "../api/user";
import JoinButton from "../components/Buttons/JoinButton";
import {useNavigate} from "react-router-dom";
import keycloak from "../keycloak";
import NewGroupDrawer from "../components/Drawers/NewGroupDrawer";


const GroupPage = () => {
    const navigate = useNavigate();
    const [groups, setGroups] = useState([]);
    const [openNewGroup, setOpenNewGroup] = useState(false);

    useEffect(() => {
        getGroups()
            .then(res => res.json())
            .then(data => {
                setGroups(data)
            })
    }, []);

    const fetchGroups = () => {
        getGroups()
            .then(res => res.json())
            .then(data => {
                setGroups(data)
            })
    };

    return (
        <>
            <div className=" flex justify-center">
                <button onClick={() => setOpenNewGroup(!openNewGroup) } type="button"
                        className=" flex justify-center mt-10 mb-8 ml-10 inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                >Add new group
                </button>
            </div>

            <NewGroupDrawer openNewGroup={openNewGroup} setOpenNewGroup={setOpenNewGroup} fetchGroups ={fetchGroups}/>
            <ul>
                {groups.map(group => {
                    return (
                        <li>
                            <div className=" flex justify-center mb-10">
                                <div className="rounded-lg shadow-lg bg-white max-w-sm">
                                    <a href="http://localhost:3000/postdetails">
                                        <img className="rounded-t-lg"
                                             src="https://mdbootstrap.com/img/new/standard/nature/184.jpg"
                                             alt=""/>
                                    </a>
                                    <div className="p-6">
                                        <h5 className="text-gray-900 text-xl font-medium mb-2">{group.fullName}</h5>
                                        <p className="text-gray-700 text-base mb-4">
                                            {group.description}
                                        </p>
                                        {group.appUser.includes(keycloak.tokenParsed.sub) && (
                                            <button type="button"
                                                    className=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                                                    onClick={() => {
                                                        navigate(`/groupdetails/${group.id}`)
                                                    }}>View
                                            </button>
                                        )}{!group.appUser.includes(keycloak.tokenParsed.sub) && (
                                        <JoinButton groupId={group.id}/>
                                    )}
                                    </div>
                                </div>
                            </div>
                        </li>
                    );
                })}
            </ul>
        </>
    )
};
export default GroupPage
